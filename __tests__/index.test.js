import React from 'react';
import App from '../src/components/app.js';
import Game from '../src/components/game/game.js';
import BoardNull from '../src/components/game/board/board_null.js';
import Square from '../src/components/game/board/squares/square.js';
import { render, shallow, mount } from 'enzyme';


describe('Testing App component', () => {
  it('should render correctly(Snapshot)', () => {
    const wrapper = render(
      <App />
    );
    expect(wrapper).toMatchSnapshot();
  });
});

describe('Testing Game component', () => {
  it('should render correctly(Snapshot)', () => {
    const wrapper = render(
      <Game />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('should render the game-board correctly', () => {
    const wrapper = mount(
      <Game />
    );
    expect(wrapper.find('.game-board').hasClass('game-board')).toEqual(true);
  });

  it('should render the game-info move list correctly', () => {
    const wrapper = mount(
        <Game />
    );
    expect(wrapper.find('.game-info').childAt(2).type()).toEqual('ol');
  });

  it('should render the game-info button \"Go to game start\" correctly', () => {
    const wrapper = mount(
        <Game />
    );
    expect(wrapper.find('.game-info').childAt(3).type()).toEqual('button');
  });

  it('should render the button \"Ordering by: { value }\" correctly on same div as \"move list\"', () => {
    const wrapper = mount(
        <Game />
    );
    expect(wrapper.find('.game-info').childAt(3).type()).toEqual('button');
  });

  it('should render the \"○ × ○ × ○ × ○ × ○ × ○ × ○ × ○ × ○\" correctly', () => {
    const wrapper = mount(
        <Game />
    );
    expect(wrapper.find('.welcomejson').hasClass('welcomejson')).toEqual(true);
  });

  it('should render the ol for the button \"Click me, bitch!\" correctly', () => {
    const wrapper = mount(
        <Game />
    );
    expect(wrapper.find('.welcomejson').childAt(3).type()).toEqual('ol');
  });
});

describe('Testing the board component', () => {
  it('should render the game board with null values correctly', () => {
    const wrapper = shallow(
      <BoardNull />
    );
    expect(wrapper.find('.board-col').hasClass('board-col')).toEqual(true);
  });
});

describe('Testing Square component', () => {
  it('should render component correctly(Snapshot)', () => {
    const wrapper = render(
      <Square />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('should render buttons correctly with null values in it', () => {
    const wrapper = shallow(
      <Square />
    );
    expect(wrapper.contains(<button className="square null" />)).toEqual(true);
  });

  it('should render button \"square 0\" with value \"O\" when passed', () => {
    const wrapper = shallow(
      <Square key="square 0" value="O" />
    );
    expect(wrapper.contains(<button className="square null" data-pro="O">O</button>)).toEqual(true);
  });
});

describe('Testing tic tac toe with selenium', () => {
  require('chromedriver');
  const { Builder, By } = require('selenium-webdriver');
  let driver;

  beforeEach( async () => {
    jest.setTimeout(30000);
    driver = await new Builder().forBrowser('chrome').build();
    await driver.get('http://10.12.0.27:3000/#');
  });

  afterEach( async () => {
    await driver.close();
    await driver.quit();
  });

  it('should check if title matches as \"Jogo da velha - React (:\"', async () => {
    await driver.sleep(2000).then( async () => {
      await driver.getTitle().then( (title) => {
        expect(title).toEqual('Jogo da velha - React (:');
      });
    });
  });

  it('should simulate a draw in the game', async () => {
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[1]/button[1]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[2]/button[1]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[1]/button[2]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[1]/button[3]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[2]/button[3]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[2]/button[2]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[3]/button[1]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[3]/button[3]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[3]/button[2]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[2]/span")).getText()
      .then( (text) => {
        expect(text).toEqual('Draw!');
      });
  });

  it('should simulate a winner \"X\" with the pos @ [0, 1, 2]', async () => {
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[1]/button[1]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[2]/button[1]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[1]/button[2]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[2]/button[2]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[1]/button[3]")).click()
    await driver.findElement(By.xpath("/html/body/div/div/div[2]/span")).getText()
      .then( (text) => {
        expect(text).toEqual('Winner: X with pos @ [0,1,2]');
      });
  });

  it('should simulate a \"Go to start\" click after a game which \"X\" has won', async () => {
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[1]/button[1]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[2]/button[1]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[1]/button[2]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[2]/button[2]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[1]/button[3]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[2]/ol/li[1]/button")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[2]/span")).getText()
      .then( (text) => {
        expect(text).toEqual('Next player: X');
      });
  });

  it('should simulate a click on \"move #1 and pos @ [1,1]\" correctly and the next player has to be \"O\"', async () => {
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[1]/button[1]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[2]/button[1]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[1]/button[2]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[1]/button[3]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[2]/button[3]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[2]/button[2]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[1]/div/div[3]/button[1]")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[2]/ol/li[2]/button")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[2]/span")).getText()
      .then( (text) => {
        expect(text).toEqual('Next player: O');
      });
  });

  it('should simulate a click on \"Ordering by: { value }\" button', async () => {
    await driver.findElement(By.xpath("/html/body/div/div/div[2]/button")).click();
    await driver.findElement(By.xpath("/html/body/div/div/div[2]/button")).getText()
      .then( async (text) => {
        expect(text).toEqual('Ordering by: Ascending');
      });
  });

  it('should simulate a click on \"Click me, bitch\" and the message \"Welcome to our game!\" should render on the screen',
  async () => {
    await driver.findElement(By.xpath("/html/body/div/div/div[3]/ol[1]/button")).click();
    await driver.sleep(7000).then( async () => {
      await driver.findElement(By.xpath("/html/body/div/div/div[3]/ol[2]")).getText()
      .then( async (text) => {
        expect(text).toEqual('Welcome to our game!');
      });
    });
  });
});
