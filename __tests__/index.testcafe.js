import { Selector, wait } from 'testcafe';


fixture('Testing tic tac toe with testcafe')
    .page("http://10.12.0.27:3000/#");

test('should check if title matches as \"Jogo da velha - React (:\"', async t => {
  const title = Selector('title');
  await t
    .expect(title.innerText).eql('Jogo da velha - React (:');
});

test('should simulate a click on \"Ordering by: { value }\" button', async t => {
  const element = Selector('button[class="btn btn-outline-secondary medium-history"]');
  await t
    .click(element)
    .expect(Selector('button[class="btn btn-outline-secondary medium-history"]').withText('Ordering by: Ascending').exists).ok();
});

test('should simulate a click in \"Click me, bitch!\" button and the message \"Welcome to our game!\" should render on the screen',
async t => {
  const element = Selector('button[class="btn btn-outline-dark small-json"]');
  await t
    .click(element)
    .wait(6000);
  await t.expect(Selector('ol').withText('Welcome to our game!').exists).ok();
});
