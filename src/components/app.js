import React from 'react';
import Game from './game/game.js';

class App extends React.Component {
  render() {
    return (
        <Game />
    );
  }
}

export default App;
