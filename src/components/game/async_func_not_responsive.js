import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { action } from '@storybook/addon-actions';


class AsyncF extends React.Component {
  constructor(props) {
    super(props);
      this.state = {
        welcome: [],
        loading: false
      };
  }

  welcome_json() {
    this.setState({
      loading: true
    });
    setTimeout( () => {
      this.setState({
        loading: false
      });
    }, 3000);
  }

  render() {
    const { loading } = this.state.loading;

    return (
      <div className="welcomejson">
        <span className="badge badge-danger small-status">
        ○ × ○ × ○ × ○ × ○ × ○ × ○ × ○ × ○
        </span><br/><br/>
        <ol><button onClick={ () => this.welcome_json() }
        disabled={ loading }
        type="button" className="btn btn-outline-dark small-json">
        { this.state.loading === false ?
        'Click me, bitch!' :
        <div className="spinner-border spinner-border-sm" role="status">
        <span className="sr-only"></span>
        </div> }
        </button></ol>
      </div>
);
}
}

export default AsyncF;
