import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { action } from '@storybook/addon-actions';


class AsyncFunc extends React.Component {
  constructor(props) {
    super(props);
      this.state = {
welcome: [],
        loading: false
      };
  }

  async welcome_json() {
    const axios = require('axios');
    this.setState({
      loading: true,
      welcome: []
    });
    await axios.get('http://www.mocky.io/v2/5ce40f28310000a05e742b64?mocky-delay=5000ms')
      .then( response => {
        this.setState({
          welcome: response.data
        });
      }).catch( () => {
        this.setState({
          loading: true
        });
      });
      this.setState({
        loading: false
      });
  }

  render() {
    const { loading } = this.state.loading;

    return (
      <div className="welcomejson" onClick={null}>
        <ol><button onClick={ () => this.welcome_json() } disabled={ loading }
        type="button" className="btn btn-outline-dark small-json">
        { this.state.loading === false ?
        'Click me, bitch!' :
        <div className="spinner-border spinner-border-sm" role="status">
        <span className="sr-only"></span>
        </div> }
        </button></ol>
        <ol>{ this.state.welcome.map(response => response.message) }</ol>
      </div>
);
}
}

export default AsyncFunc;
