import React from 'react';
import Square from './squares/square.js';


class Board extends React.Component {
  renderSquare(i) {
    return (
      <Square key={"square " + i}
        isWinning={ this.props.winningSquares.includes(i) }
        value={ this.props.squares[i] }
        onClick={ () => this.props.onClick(i) }/>
      );
  }

  render() {
    let rows = [];
    for (let i = 0; i < 3; i++) {
      let cols = [];
      for (let j = 0; j < 3; j++) {
        cols.push(this.renderSquare(i*3 + j));
      }
      rows.push(<div key={i} className="board-row">{ cols }</div>);
    }
    return (
      <div className="board-col">{ rows }</div>
    );
  }
}

export default Board;
