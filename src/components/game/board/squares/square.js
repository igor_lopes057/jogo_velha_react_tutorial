import React from 'react';


function Square(props) {
    return (
      <button
        className={"square " + (props.isWinning ? "square-wining" : null)}
        data-pro={props.value} onClick={props.onClick}>
          {props.value}
      </button>
    );
  }

export default Square;
