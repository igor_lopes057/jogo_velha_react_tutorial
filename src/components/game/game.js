import React from 'react';
import Board from './board/board.js';
import 'bootstrap/dist/css/bootstrap.min.css';


function calculateWinner(squares) {
  const lines = [
   [0, 1, 2],
   [3, 4, 5],
   [6, 7, 8],
   [0, 3, 6],
   [1, 4, 7],
   [2, 5, 8],
   [0, 4, 8],
   [2, 4, 6],
 ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return { player: squares[a], line: [a, b, c] };
    }
  }
  return null;
}

class Game extends React.Component {
  constructor(props) {
    super(props);
      this.state = {
        history: [{
          squares: Array(9).fill(null),
        }],
        stepNumber: 0,
        xIsNext: true,
        isDescending: true,
        welcome: [],
        loading: false
      };
  }

  async welcome_json() {
    const axios = require('axios');
    this.setState({
      loading: true,
      welcome: []
    });
    await axios.get('http://www.mocky.io/v2/5ce40f28310000a05e742b64?mocky-delay=5000ms')
      .then( response => {
        this.setState({
          welcome: response.data
        });
      }).catch( () => {
        this.setState({
          loading: true
        });
      }).finally( () => {
        this.setState({
          loading: false
        });
      });
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    const localization = [
      [1, 1],
      [1, 2],
      [1, 3],
      [2, 1],
      [2, 2],
      [2, 3],
      [3, 1],
      [3, 2],
      [3, 3]
    ];

    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
      history: history.concat([{
        squares: squares,
        localization: localization[i]
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0
    });
  }

  sort_history() {
    this.setState({
      isDescending: !this.state.isDescending
    });
  }

  render() {
    const { loading } = this.state.loading;
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = calculateWinner(current.squares);

    const moves = history.map((step, move) => {
      const desc = move ?
        'Go to move #' + move +
        ' and pos @ [' + history[move].localization + ']':
        'Go to game start';
    return(
      <li key={move}>
        <button type="button" className="btn btn-light medium-move"
        onClick={() => this.jumpTo(move)}>
        {move === this.state.stepNumber ? <b><i>{desc}</i></b> : <i>{desc}</i>}
      </button>
      </li>
    );
  });

    let status;
    if(winner) {
      status = 'Winner: ' + winner.player + " with pos @ [" + winner.line + "]";
    } else if(!current.squares.includes(null)) {
      status = 'Draw!';
    } else {
      status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board winningSquares={winner ? winner.line : []}
            squares={current.squares}
            onClick={i => this.handleClick(i)}/>
        </div>
        <div className="game-info">
          <span className="badge badge-success small-status">{status}</span><br/>
          <ol>{this.state.isDescending ? moves : moves.reverse()}</ol>
          <button type="button" className="btn btn-outline-secondary medium-history"
          onClick={ () => this.sort_history() }>
            Ordering by: {this.state.isDescending ? 'Descending' : 'Ascending'}
          </button>
        </div>
        <div className="welcomejson">
          <span className="badge badge-danger small-status">
          ○ × ○ × ○ × ○ × ○ × ○ × ○ × ○ × ○
          </span><br/><br/>
          <ol><button onClick={ () => this.welcome_json() } disabled={ loading }
          type="button" className="btn btn-outline-dark small-json">
            { this.state.loading === false ?
            'Click me, bitch!' :
            <div className="spinner-border spinner-border-sm" role="status">
              <span className="sr-only"></span>
            </div> }
          </button></ol>
          <ol>{ this.state.welcome.map(response => response.message) }</ol>
        </div>
      </div>
    );
  }
}

export default Game;
