import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import App from '../src/components/app.js';
import GameInfo from '../src/components/game/game_info_not_responsive.js';
import AsyncF from '../src/components/game/async_func_not_responsive.js';
import AsyncFunc from '../src/components/game/async_func_resp_to_storybook.js';
import BoardNull from '../src/components/game/board/board_null.js';
import Square from '../src/components/game/board/squares/square.js';
import '../src/index.css';


storiesOf('App', module)
  .add('Working (:', () => (
    <a href="http://10.12.0.27:3000/#">Click here!</a>
  ));

storiesOf('Welcome', module)
  .add('Inicial state', () => (
    <AsyncF />
  ))
  .add('Button and message after delay', () => (
    <AsyncFunc />
  ));

storiesOf('Game info', module)
.add('Inicial state', () => (
  <GameInfo />
));

storiesOf('Game board', module)
  .add('Null state board', () => (
    <BoardNull key="Null"
          onClick={ action('Clicked!') } />
  ));

storiesOf('Square Component', module)
  .add('Null state', () => <Square onClick={ action('Clicked!') }/> )
  .add('With X value in its state', () => (
    <Square key="square 0" value="X" onClick={ action('Already a value in it :)') }/>
  ))
  .add('With O value in its state', () => (
    <Square key="square 0" value="O" onClick={ action('Already a value in it :)') }/>
  ));
